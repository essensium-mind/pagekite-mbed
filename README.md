# pagekite-mbed
> Mbed library to create a Pagekite tunnel to a Pagekite frontend.

This is a library which provides [Pagekite](https://pagekite.net/) [Mbed](https://www.mbed.com/) backend integration. It has been tested with mbed-cli on an [LPC1768 Mbed platform](https://os.mbed.com/platforms/mbed-LPC1768/).

## Installation

To use this library in your project,  this repository, clone it using `mbed add`:
```sh
    mbed add git@gitlab.com:essensium-mind/pagekite-mbed.git pagekite
```
## Usage example
An example project can be found at [https://gitlab.com/essensium-mind/pagekite-mbed-test](https://gitlab.com/essensium-mind/pagekite-mbed-test).

## Limitations

* Currently only works over HTTP, SSL/TLS is not supported.
* Only allows 1 client connection at the same time.

## Meta
[Essensium - Mind](https://www.mind.be/)

* Bert Outtier
* Arnout Vandecappelle

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/essensium-mind/pagekite-mbed](https://gitlab.com/essensium-mind/pagekite-mbed)

## Contributing

1. Fork it (<https://gitlab.com/yourname/yourproject/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

## Why not use libpagekite?

A library for interacting with pagekite already exists: 
[libpagekite](https://github.com/pagekite/libpagekite). Why not reause that
library? The developers considered that too complicated, for the following
reasons:

1. libpagekite is bound fairly tightly with OpenSSL, which is not available in mbed.
2. libpagekite uses libev, which has not yet been ported to mbed.
3. libpagekite uses a lot of hosted standard C library (e.g. fdopen(), fprintf()) which is not readily available in mbed.
4. libpagekite uses standard UNIX sockets, which are not available in mbed.
... (a few more POSIX things that are not available in mbed
5. mbed has a pretty specific way of integrating libraries that doesn't play well with CMake or autoconf. Fairly easy to work around but messy.

The developers decided that it would be less effort to start from scratch rather
than trying to overcome all these problems.