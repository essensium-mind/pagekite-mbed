/*
 * PagekiteBackend.cpp
 *
 * Copyright 2017 Arnout Vandecappelle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included
 *   in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "PagekiteBackend.h"
#include <stdio.h>
#include <stdlib.h>
#include <mbedtls/sha1.h>

#if 0
//Enable debug
#include <cstdio>
#define DBG(x, ...) std::printf("[PK : DBG] "x"\r\n", ##__VA_ARGS__);
#define WARN(x, ...) std::printf("[PK : WARN] "x"\r\n", ##__VA_ARGS__);
#define ERR(x, ...) std::printf("[PK : ERR] "x"\r\n", ##__VA_ARGS__);

#else
//Disable debug
#define DBG(x, ...)
#define WARN(x, ...)
#define ERR(x, ...)

#endif

// CONSTRUCTOR
PagekiteBackend::PagekiteBackend(const char *kitename, uint16_t public_port, const char *secret)
    : _kitename(kitename), _public_port(public_port), _secret(secret) {
		
    static const char personalization[] = "my_app_specific_string";
    mbedtls_entropy_init(&_entropy);

    mbedtls_ctr_drbg_init(&_ctr_drbg);

    int ret = mbedtls_ctr_drbg_seed(&_ctr_drbg , mbedtls_entropy_func, &_entropy, (const unsigned char *)personalization, strlen(personalization));
    if(ret != 0) {
        ERR("Unable to seed rng");
    }
}

nsapi_error_t PagekiteBackend::doConnect() {
    nsapi_error_t err;
    static frame_t response;

    err = _frontend_socket.connect(_kitename, _frontend_port);
    if (err != NSAPI_ERROR_OK) {
        ERR("Error connecting: %d", err);
        _frontend_socket.close();
        return err;
    }

    static char salt[37];
    get_random(salt, 36);

    int line_length = snprintf(_sendBuffer, _sendBuffer_size,
                               "CONNECT PageKite:1 HTTP/1.0\r\n"
                               "X-PageKite-Features: AddKites\r\n"
                               "X-PageKite: %s-%u:%s:%s:%s:%s\r\n\r\n",
                               /* protocol */ "raw",
                               /* port     */ _public_port,
                               /* name     */ _kitename,
                               /* bsalt    */ salt,
                               /* fsalt    */ "",
                               /* sig      */ "");
    MBED_ASSERT((size_t)line_length < _sendBuffer_size);
    err = sendToFrontend(_sendBuffer, line_length);
    if (err != NSAPI_ERROR_OK) {
        ERR("Error sending to frontend: %d", err);
        return err;
    }

    memset(_recvBuffer, 0, _recvBuffer_size);
    err = receiveHeaders(&response);
    if(err != NSAPI_ERROR_OK) {
        ERR("Error receiving headers: %d", err);
        return err;
    }

    err = parseFrame(&response);
    if(err != NSAPI_ERROR_OK) {
        ERR("Error parsing headers: %d", err);
        return err;
    }

    while(response.result == PAGEKITE_SIGNTHIS) {
        static char signature[37];
        sign(response.payload, signature);
        int length = snprintf(_sendBuffer, sizeof(_sendBuffer), "NOOP: 1\r\nX-PageKite: %s:%s\r\n\r\n",
            /* payload  */ response.payload,
            /* sig      */ signature);

        err = sendChunk(length);
        if(err != NSAPI_ERROR_OK) {
            ERR("Send sign failed: %d", err);
            return err;
        }
        err = recvFrame(&response);
        if(err != NSAPI_ERROR_OK) {
            ERR("Second connect failed: %d", err);
            return err;
        }
    }

    if(response.result == PAGEKITE_INVALID) {
        return NSAPI_ERROR_PARAMETER;
    }

    if(response.result == PAGEKITE_DUPLICATE) {
        return NSAPI_ERROR_PARAMETER;
    }

    if(response.result == PAGEKITE_ERROR) {
        return NSAPI_ERROR_PARAMETER;
    }

    // response.result == PAGEKITE_OK
    //set session ID?
    return NSAPI_ERROR_OK;
};

nsapi_error_t PagekiteBackend::sendPing() {
    static const char noopChunk[] = "NOOP: 1\r\nPING: 1\r\n\r\n";
    strcpy(_sendBuffer, noopChunk);
    return sendChunk(sizeof(noopChunk)-1);
}

nsapi_error_t PagekiteBackend::sendPong() {
    static const char noopChunk[] = "NOOP: 1\r\nPONG: 1\r\n\r\n";
    strcpy(_sendBuffer, noopChunk);
    return sendChunk(sizeof(noopChunk)-1);
}

nsapi_error_t PagekiteBackend::sendToFrontend(const void *data, nsapi_size_t size) {
    DBG("Send <<<<\n%.*s>>>>", size, data);
    nsapi_size_or_error_t sent = _frontend_socket.send(data, size);
    if (sent != (nsapi_size_or_error_t)size) {
        _frontend_socket.close();
        if (sent < 0) {
            ERR("Connection closed: %d", sent);
            return sent;
        }
        else {
            ERR("Connection lost");
            return NSAPI_ERROR_CONNECTION_LOST;
        }
    }
    return NSAPI_ERROR_OK;
}

nsapi_error_t PagekiteBackend::sendChunk(nsapi_size_t size) {
    static char chunkHeader[10]; /* Max 7 digits */
    int line_length = snprintf(chunkHeader, sizeof(chunkHeader), "%x\r\n", size);
    MBED_ASSERT((size_t)line_length < sizeof(chunkHeader));
    nsapi_error_t err = sendToFrontend(chunkHeader, line_length);
    if (err != NSAPI_ERROR_OK) {
        return err;
    }
    return sendToFrontend(_sendBuffer, size);
}

nsapi_error_t PagekiteBackend::receiveHeaders(frame_t *frame) {
    nsapi_size_t rcount = 0;
    while(strstr(_recvBuffer, "\r\n\r\n") == NULL) {
        if(rcount < _recvBuffer_size - 1) {
            nsapi_size_or_error_t extra = _frontend_socket.recv(_recvBuffer+rcount, _recvBuffer_size - rcount - 1);
            if(extra < 0) {
                return extra;
            }
            else if(extra == 0) {
                return NSAPI_ERROR_NO_CONNECTION;
            }
            else {
                rcount += extra;
            }
        }
        else {
            return NSAPI_ERROR_NO_MEMORY;
        }
    }
    _recvBuffer[rcount] = '\0';

    /* Data starts just after the HTTP header */
    frame->chunkData = strstr(_recvBuffer, "\r\n");
    if(frame->chunkData == NULL) {
        ERR("No HTTP header?");
        return NSAPI_ERROR_PARAMETER;
    }

    *frame->chunkData = '\0';
    frame->chunkData += 2;

    int httpResponseCode = 0;
    if( sscanf(_recvBuffer, "HTTP/1.%*d %d", &httpResponseCode) != 1 )
    {
        ERR("Not a correct HTTP answer : %s", _recvBuffer);
        return NSAPI_ERROR_PARAMETER;
    }

    if( (httpResponseCode < 200) || (httpResponseCode >= 300) )
    {
        WARN("Response code %d", httpResponseCode);
        return NSAPI_ERROR_PARAMETER;
    }

    frame->length = rcount - (frame->chunkData - _recvBuffer);
    return NSAPI_ERROR_OK;
}

nsapi_error_t PagekiteBackend::recvFrame(frame_t *frame) {
    nsapi_error_t err;

    while (true) {
        err = recv(frame);
        if(err != NSAPI_ERROR_OK) {
            ERR("Could not receive new frame: %d", err);
            return err;
        }

        err = parseFrame(frame);
        if(err != NSAPI_ERROR_OK) {
            ERR("Could not parse frame: %d", err);
            return err;
        }

        if(frame->chunk.PING) {
            DBG("PING received, sending PONG");
            err = sendPong();
            if(err != NSAPI_ERROR_OK) {
                ERR("Could not send PONG: %d", err);
            }
            continue;
        }

        if(frame->chunk.PONG) {
            DBG("PONG received");
            continue;
        }

        if(frame->chunk.ZRST) {
            DBG("ZRST received");
            continue;
        }

        return err;
    }
}

nsapi_error_t PagekiteBackend::recv(frame_t *frame) {
    nsapi_size_t rcount = 0;
    int length = -1;
    char *data = NULL;
    while((int)rcount < length || length < 0) {
        nsapi_size_or_error_t extra;
        if(length < 0) {
            extra = _frontend_socket.recv(_recvBuffer+rcount, _recvBuffer_size - rcount);
        }
        else {
            extra = _frontend_socket.recv(_recvBuffer+rcount, length - rcount);
        }

        if(extra < 0) {
            ERR("Error when receiving: %d", extra);
            return extra;
        }
        else if (extra == 0) {
            ERR("Connection closed while receiving");
            return NSAPI_ERROR_NO_CONNECTION;
        }
        else {
            rcount += extra;
        }

        DBG("Received <<<<\n%.*s>>>>", rcount, _recvBuffer);

        if(length < 0) {
            if(rcount > 2) {
                if(strstr(_recvBuffer, "\r\n") != NULL && length < 0) {
                    int position = zero_first_crlf(rcount, _recvBuffer);
                    if (1 != sscanf(_recvBuffer, "%x", &length)) {
                        ERR("Bad frame");
                        return NSAPI_ERROR_NO_MEMORY;
                    }
                    else {
                        if(length > (int)_recvBuffer_size) {
                            ERR("Frame length is bigger than receive buffer");
                            return NSAPI_ERROR_NO_MEMORY;
                        }
                        else {
                            data = _recvBuffer+position;
                        }
                    }
                }
            }
        }
        else {
            if((int)rcount >= length) {
                return NSAPI_ERROR_NO_MEMORY;
            }
        }
    }

    frame->chunkData = data;
    frame->length = length;
    return NSAPI_ERROR_OK;
}

nsapi_error_t PagekiteBackend::parseFrame(frame_t *frame) {
    int nextIndex=0;
    int remainingLength = frame->length;
    char *workBuffer = frame->chunkData;

    // Initialise default values of frame
    frame->chunk.NOOP = false;
    frame->chunk.endOfStream = false;
    frame->chunk.PING = false;
    frame->chunk.PONG = false;
    frame->chunk.ZRST = false;
    frame->result = PAGEKITE_ERROR;

    char *value;

    while (nextIndex < remainingLength) {
        remainingLength -= nextIndex;

        if(workBuffer[0] == '\r' && workBuffer[1] == '\n') {
            // end of frame headers, now follows user data
            frame->chunk.data = workBuffer + 2;
            frame->chunk.length = remainingLength - 2;
            break;
        }

        nextIndex = zero_first_crlf(remainingLength, workBuffer);
        if(nextIndex == 0) break;

        value = strchr(workBuffer, ':') + 2;

        if(strncmp(workBuffer, "X-PageKite-SessionID: ", 22) == 0) {
            DBG("session Id: %s", value);
            strncpy(frame->sessionId, value, 51);
        }

        if(strncmp(workBuffer, "X-PageKite-Invalid: ", 20) == 0) {
            DBG("Invalid");
            strncpy(frame->payload, value, 300);
            frame->result = PAGEKITE_INVALID;
        }

        if(strncmp(workBuffer, "X-PageKite-Duplicate: ", 22) == 0) {
            DBG("Duplicate");
            strncpy(frame->payload, value, 300);
            frame->result = PAGEKITE_DUPLICATE;
        }

        if(strncmp(workBuffer, "X-PageKite-SignThis: ", 21) == 0) {
            DBG("Signthis");
            strncpy(frame->payload, value, 300);
            frame->result = PAGEKITE_SIGNTHIS;
        }

        if(strncmp(workBuffer, "X-PageKite-OK: ", 15) == 0) {
            DBG("OK");
            frame->result = PAGEKITE_OK;
        }

        if(strncmp(workBuffer, "SID: ", 5) == 0) {
            DBG("SID: %s", value);
            frame->chunk.SID = value;
        }

        if(strncmp(workBuffer, "Host: ", 6) == 0) {
            DBG("Host: %s", value);
            frame->chunk.Host = value;
        }

        if(strncmp(workBuffer, "Proto: ", 7) == 0) {
            DBG("Proto: %s", value);
            frame->chunk.Proto = value;
        }

        if(strncmp(workBuffer, "Port: ", 6) == 0) {
            DBG("Port: %s", value);
            frame->chunk.Port = value;
        }

        if(strncmp(workBuffer, "RIP: ", 5) == 0) {
            DBG("RIP: %s", value);
            frame->chunk.RIP = value;
        }

        if(strncmp(workBuffer, "RPort: ", 7) == 0) {
            DBG("RPort: %s", value);
            frame->chunk.RPort = value;
        }

        if(strncmp(workBuffer, "NOOP: ", 6) == 0) {
            DBG("NOOP: %s", value);
            frame->chunk.NOOP = (*value == '1');
        }

        if(strncmp(workBuffer, "EOF: ", 5) == 0) {
            DBG("EOF: %s", value);
            frame->chunk.endOfStream = (*value == '1');
        }

        if(strncmp(workBuffer, "PING: ", 6) == 0) {
            DBG("PING: %s", value);
            frame->chunk.PING = (*value == '1');
        }

        if(strncmp(workBuffer, "PONG: ", 6) == 0) {
            DBG("PONG: %s", value);
            frame->chunk.PONG = (*value == '1');
        }

        if(strncmp(workBuffer, "ZRST: ", 6) == 0) {
            DBG("ZRST: %s", value);
            frame->chunk.ZRST = (*value == '1');
        }

        if(strncmp(workBuffer, "SPD: ", 5) == 0) {
            DBG("SPD: %s", value);
            frame->chunk.SPD = value;
        }

        workBuffer += nextIndex;
    }
    return NSAPI_ERROR_OK;
}

void PagekiteBackend::sign(char *payload, char *signature) {
    unsigned char sha1_output[20];

    // <sign> = <salt> + HEX(SHA1(<secret> + <payload> + <salt>))
    get_random(signature, 8);

    mbedtls_sha1_context ctx;

    mbedtls_sha1_init( &ctx );
    mbedtls_sha1_starts( &ctx );
    mbedtls_sha1_update( &ctx, reinterpret_cast<const unsigned char*>(_secret), strlen(_secret) );
    mbedtls_sha1_update( &ctx, reinterpret_cast<const unsigned char*>(payload), strlen(payload) );
    mbedtls_sha1_update( &ctx, reinterpret_cast<const unsigned char*>(signature), 8 );
    mbedtls_sha1_finish( &ctx, sha1_output );
    mbedtls_sha1_free( &ctx );

    // Convert to HEX format
    // Truncate signature at 36 characters, i.e. 28 after salt, i.e. 14 bytes of sha1_output
    for(int i=0; i<14; i++) {
        sprintf(&signature[8 + i*2], "%02x", sha1_output[i]);
    }
}

void PagekiteBackend::get_random(char *buffer, size_t length) {
    static const char alphanum[] =
        "0123456789"
        "abcdefghijklmnopqrstuvwxyz";

    int ret = mbedtls_ctr_drbg_random(&_ctr_drbg, (unsigned char *)buffer, length );
    if(ret != 0) {
        ERR("Unable to generate random");
    }

    for (int i = 0; i < length; ++i) {
        buffer[i] = alphanum[(int)buffer[i] % (sizeof(alphanum) - 1)];
    }

    buffer[length] = '\0';
}

int PagekiteBackend::zero_first_crlf(int length, char* data) {
  for (int i = 0; i < length-1; i++)
  {
    if ((data[i] == '\r') && (data[i+1] == '\n'))
    {
      data[i] = data[i+1] = '\0';
      return i+2;
    }
  }
  return 0;
}

nsapi_error_t PagekiteBackend::accept(PagekiteSocket*& connection) {

    static frame_t frame;
    nsapi_error_t err;

    do {
        frame.chunk.SID = NULL;
        frame.chunk.Proto = NULL;
        frame.chunk.Host = NULL;
        frame.chunk.Port = NULL;
        frame.chunk.RIP = NULL;
        frame.chunk.RPort = NULL;

        err = recvFrame(&frame);
        if(err != NSAPI_ERROR_OK) {
            ERR("Error receiving in accept(): %d", err);
            return err;
        }
    } while(frame.chunk.SID == NULL || frame.chunk.Proto == NULL || frame.chunk.Host == NULL || frame.chunk.Port == NULL || frame.chunk.RIP == NULL || frame.chunk.RPort == NULL);
	
    connection = new PagekiteSocket(this, frame.chunk.SID, frame.chunk.Proto, frame.chunk.Host, frame.chunk.Port, frame.chunk.RIP, frame.chunk.RPort);
    return NSAPI_ERROR_OK;
	
}

nsapi_error_t PagekiteBackend::close() {
    return _frontend_socket.close();
}
