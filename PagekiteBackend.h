/*
 * PagekiteBackend.h
 *
 * Copyright 2017 Arnout Vandecappelle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included
 *   in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#ifndef PAGEKITE_PAGEKITEBACKEND_H_
#define PAGEKITE_PAGEKITEBACKEND_H_

#include <TCPSocket.h>
#include "PagekiteSocket.h"
#include <mbedtls/entropy.h>
#include <mbedtls/ctr_drbg.h>

/**
 * Result of a CONNECT to a Pagekite frontend
 */
enum PagekiteResult
{
    PAGEKITE_ERROR=-4,
    PAGEKITE_INVALID=-3,
    PAGEKITE_DUPLICATE=-2,
    PAGEKITE_SIGNTHIS=-1,
    PAGEKITE_OK = 0,
};

/**
 * A Pagekite chunk
 */
struct chunk_t {
    char *SID;
    char *Host;
    char *Proto;
    char *Port;
    char *RIP;
    char *RPort;
    bool NOOP;
    bool endOfStream;
    bool PING;
    bool PONG;
    bool ZRST;
    char *SPD;
    int length;
    char* data;
};

/**
 * A Pagekite frame
 */
struct frame_t {
    int length;
    char *chunkData;
    char sessionId[51]; /* TODO make this a pointer */
    char payload[300]; /* TODO make this a pointer */
    PagekiteResult result;
    chunk_t chunk;
};

/**
 * Forward declaration of PagekiteSocket class
 */
class PagekiteSocket;

/** PagekiteBackend class.
 *  This class provides the Pagekite handshaking code and
 *  is able to send/receive frames over a Pagekite connection..
 */
class PagekiteBackend {
public:

    /** Create the backend of a kite.
    *
    * A backend is created to connect to one specific kite.
    * Use the connect() member to (re)establish the connection.
    *
    * @param kitename The name of the kite, e.g. mykite.pagekite.me
    * @param public_port The port on the front-end where clients connect
    * @param secret The secret of the kite
    *
    * @return Error if the connection has failed.
    *
    * @todo TLS support
    * @todo front-end handshake port
    * @todo protocol
    */
    PagekiteBackend(const char *kitename, uint16_t public_port, const char *secret);
    virtual ~PagekiteBackend() {}

    /** Connect to the pagekite frontend.
    *
    * Establish the connection to the frontend. Returns when the connection is
    * established or has failed.
    *
    * @param S The network stack used for the connection.
    * @return Error if the connection has failed.
    */
    template <typename S>
    nsapi_error_t connect(S *stack)
    {
        nsapi_error_t err = _frontend_socket.open(stack);
        if (err == NSAPI_ERROR_OK)
        return doConnect();
        else
        return err;
    }

    /** Reconnect to the pagekite frontend.
    *
    * Re-establish the connection to the frontend. Returns when the connection is
    * established or has failed.
    *
    * @param S The network stack used for the connection.
    *
    * @return Error if the connection has failed.
    */
    template <typename S>
    nsapi_error_t reconnect(S *stack)
    {
        _frontend_socket.close();
        return connect(stack);
    }

    /** Send a "PING" to the connected frontend.
    *
    * Sends a "PING" chunk to the Pagekite frontend in order to
    * verify the connection. The Pagekite frontend should answer with
    * a "PONG" chunk.
    *
    * @return Error if sending the "PING" chunk failed.
    */
    nsapi_error_t sendPing();

    /** Disconnect from the pagekite frontend.
    *
    * This also closes all pagekite connections.
    *
    * @return Error if closing the Pagekite socket failed.
    */
    nsapi_error_t close();

    /** Accept a new Pagekite connection from the Pagekite frontend.
    *
    * This method blocks until a new client connects to the Pagekite FrontEnd
    * and return a new PagekiteSocket.
    *
    * @param connection A PagekiteSocket object that was accepted.
    *
    * @return Error if accepting a new PagekiteConenction failed
    */
    nsapi_error_t accept(PagekiteSocket*& connection);

private:
    const char *_kitename;
    uint16_t _public_port;
    const char *_secret;
    static const uint16_t _frontend_port = 80;
    TCPSocket _frontend_socket;

    /** sending buffer */
    static const size_t _sendBuffer_size = 400;
    char _sendBuffer[_sendBuffer_size];
    /** receiving buffer */
    static const size_t _recvBuffer_size = 1400;
    char _recvBuffer[_recvBuffer_size];

    /** start an entropy context */
    mbedtls_entropy_context _entropy;
    mbedtls_ctr_drbg_context _ctr_drbg;

    /** Connect to a Pagekite frontend and perform handshaking */
    nsapi_error_t doConnect();
    /** Send a "PONG" chunk to the frontend */
    nsapi_error_t sendPong();

    /** Receive frame data from the frontend */
    nsapi_error_t recv(frame_t *frame);

    /** Send data to the frontend */
    nsapi_error_t sendToFrontend(const void *data, nsapi_size_t size);

    /** Send a Pagekite chunk to the connected frontend.
    *
    * Sends a Pagekite chunk to the Pagekite frontend. This function
    * the #_sendBuffer into a Pagekite frame before sending.
    *
    * @param size The size of the Pagekite chunk data to send.
    *
    * @return Error if sending the Pagekite chunk failed.
    */
    nsapi_error_t sendChunk(nsapi_size_t size);

    /** Receive and parse HTTP data from the frontend */
    nsapi_error_t receiveHeaders(frame_t *frame);

    /** Parse the headers from a response frame */
    nsapi_error_t parseFrame(frame_t *frame);

    /** Receive a Pagekite frame from the connected frontend.
    *
    * Receive a Pagekite frame from the Pagekite frontend.
    *
    * @param frame A pointer to a frame to fill with the received data..
    *
    * @return Error if receiving the Pagekite frame failed.
    */
    nsapi_error_t recvFrame(frame_t *frame);

    /** generate a signature for the Pagekite handshaking */
    void sign(char *payload, char *signature);

    /** get a random string of length length */
    void get_random(char *buffer, size_t length);

    /** Put a '\0\0' on the first occurance of '\r\n' in  the buffer */
    int zero_first_crlf(int length, char* data);

    friend class PagekiteSocket;
};

#endif /* PAGEKITE_PAGEKITEBACKEND_H_ */
